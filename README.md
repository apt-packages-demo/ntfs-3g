# ntfs-3g

Read/write NTFS driver for FUSE. https://tracker.debian.org/ntfs-3g

## Unofficial documentation
* https://www.google.com/search?q=linux+mount+ntfs
* [*How to mount partition with ntfs file system and read write access*
  ](https://linuxconfig.org/how-to-mount-partition-with-ntfs-file-system-and-read-write-access)

## Hibernation issue
### Error message
Please resume and shutdown Windows fully (no hibernation or fast restarting.)
### What can be done?
* https://www.google.com/search?q=Please+resume+and+shutdown+Windows+fully+%28no+hibernation+or+fast+restarting.%29
* [*Unable to mount Windows (NTFS) filesystem due to hibernation*
  ](https://askubuntu.com/questions/145902/unable-to-mount-windows-ntfs-filesystem-due-to-hibernation)